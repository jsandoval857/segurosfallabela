const express = require('express');
const bodyParser = require('body-parser');
const endpoints = require('./endpoints');
const swaggerDoc = require('./swaggerDoc');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

endpoints(app);
swaggerDoc(app);

// en caso de error
app.use((err, req, res, next) => console.log('error', err));

app.listen(3000, () => console.log(' Documentacion en http://localhost:3000'));