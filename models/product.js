var Product = {
    list: [{
            id: 1,
            nombre: 'Cobertura',
            sellIn: 2,
            price: 90
        },
        {
            id: 2,
            nombre: 'Mega cobertura',
            sellIn: 4,
            price: 80,
            persistent: true
        },
        {
            id: 3,
            nombre: 'Baja cobertura',
            sellIn: 3,
            price: 80
        },
        {
            id: 4,
            nombre: 'Full cobertura',
            sellIn: 2,
            price: 22
        },
        {
            id: 5,
            nombre: 'Full cobertura Super duper',
            sellIn: 2,
            price: 90,
        },
        {
            id: 6,
            nombre: 'Super avance',
            sellIn: 2,
            price: 40
        }
    ],

    get all() {
        return this.list;
    },
    set push(product) {
        let id = 1 + this.list.length;
        this.list.push({ id, ...product })
    },
    byId(id) {
        return this.list.find(pro => pro.id == id);
    }
}

module.exports = Product;