/**
 * @swagger
 * definitions:
 *   Product:
 *     properties:
 *       nombre:
 *         type: string
 *       sellIn:
 *         type: integer
 *       price:
 *         type: integer
 * /product:
 *      get:
 *           tags:
 *               - Modulo de Productos
 *           description: Retorna todos los productos
 *           produces:
 *               - application/json
 *           responses:
 *               200:
 *                   description: Un array de productos
 *                   schema:
 *                      $ref: '#/definitions/Product'
 *      post:
 *           tags:
 *               - Modulo de Productos
 *           description: Agrega nuevo producto
 *           produces:
 *               - application/json
 *           parameters:
 *               - name: Product
 *                 description: Campos para el producto
 *                 in: body
 *                 required: true
 *                 schema:
 *                      $ref: '#/definitions/Product'
 *           responses:
 *               200:
 *                   description: Successfully created
 */

// Cargamos el módulo de express para poder crear rutas
const express = require('express');
var router = express.Router();

// Cargamos el modelo para usarlo posteriormente
let Product = require('../models/product');

// Creamos las rutas
router.route('/')
    .get(function(req, res) {
        // Devolvemos una respuesta en JSON
        res.status(200).send({
            data: Product.all
        });
    })
    .post(function(req, res) {

        Product.push = req.body;

        // Devolvemos una respuesta en JSON
        res.status(200).send({
            description: 'Successfully created'
        });
    })

module.exports = router;