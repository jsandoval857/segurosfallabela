/**
 * @swagger
 * /simulator/{days}:
 *      get:
 *           tags:
 *               - Listar ventas
 *           description: Retorna todas las ventas
 *           produces:
 *               - application/json
 *           parameters:
 *              - name: days
 *                description: Dias a simular
 *                in: path
 *                required: true
 *                type: integer
 *           responses:
 *               200:
 *                   description: un array de ventas
 *                   schema:
 *                      $ref: '#/definitions/Product'
 */

// Cargamos el módulo de express para poder crear rutas
const express = require('express');
var router = express.Router();

// Cargamos el modelo para usarlo posteriormente
let Product = require('../models/product');
let Sale = require('../models/sale');

// Creamos las rutas
router.route('/:days')
    .get(function(req, res) {
        let days = req.params.days;
        let product = Product.all;
        let data = [];

        for (let i = 0; i < days; i++) {
            data.push({
                title: ` --- Dia ${i} --- `,
                product
            })
        }

        // Devolvemos una respuesta en JSON
        res.status(200).send({
            data
        });
    })

module.exports = router;