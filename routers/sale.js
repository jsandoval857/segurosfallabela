/**
 * @swagger
 * definitions:
 *   Sale:
 *     properties:
 *       productId:
 *         type: integer
 * /sale:
 *      get:
 *           tags:
 *               - Modulo de Ventas
 *           description: Retorna todas las ventas
 *           produces:
 *               - application/json
 *           responses:
 *               200:
 *                   description: un array de ventas
 *      post:
 *           tags:
 *               - Modulo de Ventas
 *           description: Realizar nueva venta
 *           produces:
 *               - application/json
 *           parameters:
 *               - name: Venta
 *                 description: Campos para la venta
 *                 in: body
 *                 required: true
 *                 schema:
 *                      $ref: '#/definitions/Sale'
 *           responses:
 *               200:
 *                   description: Successfully created
 */

// Cargamos el módulo de express para poder crear rutas
const express = require('express');
var router = express.Router();

// Cargamos el modelo para usarlo posteriormente
let Product = require('../models/product');
let Sale = require('../models/sale');

// Creamos las rutas
router.route('/')
    .get(function(req, res) {
        // Devolvemos una respuesta en JSON
        res.status(200).send({
            data: Sale
        });
    })
    .post(function(req, res) {
        let id = 1 + Sale.length;
        let productId = req.body.productId;
        let product = Product.byId(productId);

        if (!product) {
            res.status(400).send({
                description: `Producto ${productId} no encontrado.`
            });
        }

        Sale.push({
            id,
            product
        });

        // Devolvemos una respuesta en JSON
        res.status(200).send({
            description: 'Successfully created'
        });
    })

module.exports = router;