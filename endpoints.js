// Importamos las rutas
const productRouters = require('./routers/product');
const saleRouters = require('./routers/sale');
const simulatorRouters = require('./routers/simulator');

const initializeEndpoints = (app) => {
    app.use('/product', productRouters);
    app.use('/sale', saleRouters);
    app.use('/simulator', simulatorRouters);
}

module.exports = initializeEndpoints;