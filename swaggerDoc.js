// Cargamos el módulo de swagger para la documentacion
const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');

const options = {
    swaggerDefinition: {
        info: {
            title: 'Api test Seguro fallabela',
            version: '1.0.0',
            description: 'Api con documentacion'
        },
        basePath: '/'
    },
    apis: ['routers/*.js']
};

const specs = swaggerJsdoc(options);

module.exports = (app) => {
    app.use('/', swaggerUi.serve, swaggerUi.setup(specs));
}